import json

# This is the output of the scanner that will be parsed by GitLab.
report = {
  "version": "14.1.1",
  "scan":{
      "scanner":{
          "id":"sonar_scan",
          "name":"Sonar",
          "vendor": {
              "name":"Sonar"
          },
          "version":"1.0"
        },
      "start_time":"2022-04-07T12:26:58",
      "end_time":"2022-04-26T12:26:00",
      "status":"success",
      "messages": [
        ],
      "type":"sast"
    },
  "vulnerabilities": [
    {
      "category": "A category",
      "name": "Chris's custom vulnerability",
      "message": "There is a vulnerability... somewhere.",
      "description": "Let's make a long-form description to explain the problem and why it is a vulnerability",
      "cve": "Another unique fingerprint value - asdnmvxzaslk391011",
      "severity": "High",
      "scanner": {
        "id": "sonar_scanner",
        "name": "Sonar"
      },
      "location": {
        "file": "src/myapp.c",
        "start_line": 8,
        "dependency": {
          "package": {}
        }
      },
      "identifiers": [
        {
          "type": "my_scan_id",
          "name": "MyScan ID: 17",
          "value": "17"
        }
      ]
    },
    {
      "category": "A category",
      "name": "Chris's second custom vulnerability",
      "message": "There is ANOTHER vulnerability... somewhere.",
      "description": "Let's make a long-form description to explain the problem and why it is a vulnerability",
      "cve": "A unique fingerprint value - klsnmllfgg298390rp1fli2ifh",
      "severity": "Medium",
      "scanner": {
        "id": "sonar_scanner",
        "name": "Sonar"
      },
      "location": {
        "file": "src/myapp.c",
        "start_line": 20,
        "dependency": {
          "package": {}
        }
      },
      "links": [
        {
          "name":"Database queries should not be vulnerable to injection attacks",
          "url": "https://sonarcloud.io/project/issues?resolved=false&branch=introduce-new-vuln&id=ChrisHvdSS_Vuln-PHP-petit-projet&open=AYFIypRiOItHcd49mYMV"
        }
      ],
      "identifiers": [
        {
          "type": "my_scan_id",
          "name": "MyScan ID: 18",
          "value": "18"
        }
      ]
    }
  ],
  "remediations": []
}

# Print a JSON string of the report above
print(json.dumps(report))

