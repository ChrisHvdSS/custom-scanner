#!/bin/bash

# Get the schema files
curl -s https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/archive/master/security-report-schemas-master.zip?path=dist > schemas.zip

# Extract the schema files
mkdir schemas 2> /dev/null # Quiet mode
unzip -q -o -j -d schemas schemas.zip

# Validate the report against the schema
jsonschema -i $1 schemas/sast-report-format.json
